<?php
/**
 * Template Name: Solid Service Page Template
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. The front page template
 * in Twenty Twelve consists of a page content area for adding text, images, video --
 * anything you'd like -- followed by front-page-only widgets in one or two columns.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>
<section class="page-title gray">
 <div class="header-cross">&nbsp;</div>
<div class="cross-a bottom"><div class="cross white deco-top"></div></div>
  <div class="wrap">
      <div class="wrapper">
          <h1><?php the_title(); ?></h1>
        </div>
    </div>
</section>

<?php query_posts("post_type=service&posts_per_page=-1"); ?>
<?php $i=1; ?>
<?php while(have_posts()):the_post(); ?>
<?php if($i%2!=0) { ?>
<?php if($i!=1) { ?>
<section class="services-page white">
<div class="cross-a top"><div class="cross grays"></div></div>
<?php } ?>
  <div class="wrap">
      <div class="wrapper">
      <img src="<?php echo get_field('innerimg'); ?>" class="alignleft" />
         <!-- <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id( $post->ID )); ?>" class="alignleft" > -->
      <h2><?php the_field("services_h1"); ?></h2>
            <?php the_field("innerdetail"); ?>
            <a href="<?php the_permalink(); ?>" class="read-more">Read More</a>
        </div>
    </div>
</section>
<?php } else { ?>
<section class="services-page gray">
<div class="cross-b top"><div class="cross white deco-bottom"></div></div>
  <div class="wrap">
      <div class="wrapper">
          <img src="<?php echo get_field('innerimg'); ?>" class="alignright" />
      <h2><?php the_field("services_h1"); ?></h2>
            <?php the_field("innerdetail"); ?>
            <a href="<?php the_permalink(); ?>" class="read-more">Read More</a>
        </div>
    </div>
</section>
<?php } ?>
<?php $i++; ?>
<?php endwhile; wp_reset_query(); ?>
<?php get_footer(); ?>