<?php
/**
 * Template Name: Front Page Template
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. The front page template
 * in Twenty Twelve consists of a page content area for adding text, images, video --
 * anything you'd like -- followed by front-page-only widgets in one or two columns.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>
<section class="banner">
  <div class="header-cross">&nbsp;</div>
  <div class="wrap">
    <div class="cycle-slideshow" 
        data-cycle-fx=fade
        data-cycle-timeout=0
        data-cycle-slides="> div"  
        data-cycle-pager="#pager"   
        data-cycle-pager-template=""      
        >
       <?php query_posts("post_type=banner&posts_per_page=-1&order=Desc"); ?>
       <?php while(have_posts()):the_post(); ?>
        <div class="cycle-slide"> 
            <?php the_post_thumbnail("full"); ?>
            <div class="wrapper">
              <h2><?php the_title(); ?></h2>
              <?php the_content(); ?>
            </div>
        </div>
      <?php endwhile; ?>
    </div>
    <div id="pager"> 
    <?php query_posts("post_type=banner&posts_per_page=-1&order=Desc"); ?>
      <?php while(have_posts()):the_post(); ?>
         <a href="#"><img src="<?php the_field("img"); ?>" alt="banner"></a>
      <?php endwhile; ?>
    </div>
  </div>
  <div class="cross-b bottom"><div class="cross white"></div></div>
</section>
<!-- our services start -->
<section class="home-services">
  <div class="wrap">
      <div class="wrapper">
      <div class="carousel" id="c1">
              <h1>Our Services</h1>
                <a data-id="c1" class="prev" href="JavaScript:void(0);"></a>
                <a data-id="c1" class="next" href="JavaScript:void(0);"></a>
                <?php query_posts("post_type=service&posts_per_page=-1"); ?>
              <ul>
                <?php while(have_posts()):the_post(); ?>
                  <li>
                    <?php the_post_thumbnail(array(130,115)); ?>
                        <span><?php the_field("short_title");?></span>
                        <?php the_field("short");?>
                        <a href="<?php the_permalink(); ?>">Read more</a>
                  </li>
                <?php endwhile; wp_reset_query(); ?>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- our services close -->
<!-- home about start -->
<section class="home-about gray">
<div class="cross-a top"><div class="cross white deco-bottom"></div></div>  
<div class="cross-b bottom"><div class="cross white"></div></div>
  <div class="wrap">
      <div class="wrapper"> 
      <?php while(have_posts()):the_post(); ?>        
        <?php the_content(); ?>
      <?php endwhile; ?>
        </div>
    </div>
</section>
<!-- home about close -->
<!-- home solid lock tips start -->
<section class="home-tips">
  <div class="wrap">
      <div class="wrapper">
          <h2>Solid Lock Tips</h2>
            <a href="<?php echo get_site_url(); ?>/solid-lock-tips" class="view"><img src="<?php echo get_template_directory_uri(); ?>/images/view-all-btn.png" alt="view"></a>
            <ul class="blog-post">
            <?php query_posts("post_type=post&posts_per_page=2&orderby=date&order=Desc"); ?>
            <?php while(have_posts()):the_post(); ?> 
              <li>
                  <div class="post-left">
                    <div class="post-thumb">
                      <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(array(180,180)); ?></a>
                    </div>
                  </div>
                  <div class="post-right">
                    <h3 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                    <div class="post-meta">
                      <span class="author">Post By : <a href="#"><?php the_author(); ?></a></span>
                        <span class="date"><?php the_time("d, M Y"); ?></span>
                        <span class="comments">Comments (<?php echo comments_number('0','1','%'); ?>)</span>
                    </div>
                <div class="post-content">
                      <p><?php echo get_excerpt("150"); ?></p>
                        <a href="<?php the_permalink(); ?>" class="more">Read More</a>
                    </div>
                    </div>
                </li>
                 <?php endwhile; wp_reset_query(); ?>
            </ul>
        </div>
    </div>
</section>
<!-- home solid lock tips close -->
<!-- contact now start -->
<section class="contact-now">
<div class="cross-a top"><div class="cross white"></div></div>
  <div class="wrap">
      <div class="wrapper">
      <h3><?php the_field("footertext"); ?></h3>
            <a href="<?php echo get_site_url(); ?>/contact-us"><img src="<?php echo get_template_directory_uri(); ?>/images/contact-now-btn.png" alt="contact-now"></a>
        </div>
    </div>
</section>
<!-- contact now close -->
<?php get_footer(); ?>