<?php
/**
 * Template Name: Solid lock tips Page Template
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. The front page template
 * in Twenty Twelve consists of a page content area for adding text, images, video --
 * anything you'd like -- followed by front-page-only widgets in one or two columns.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>
<!-- page title start -->
<section class="page-title gray blog">
 <div class="header-cross">&nbsp;</div>
<div class="cross-a bottom"><div class="cross white deco-top"></div></div>
  <div class="wrap">
      <div class="wrapper">
          <h1><?php the_title(); ?></h1>
            <div class="widget-area">
              <ul class="widget-control">
                  <li><a href="<?php echo get_site_url(); ?>/popular-post">Popular Post</a></li>
                    <li><a href="<?php echo get_site_url(); ?>/recent-post">Recent Post</a></li>
                    <li>
                      <div class="widget-category">
                      <select>
                            <option value="option1">Category</option>
                            <option value="option2">Option 2</option>
                            <option value="option3">Option 3</option>
                            <option value="option4">Option 4</option>
                            <option value="option5">Option 5</option>
                            <option value="option6">Option 6</option>
                            <option value="option7">Option 7</option>
                            <option value="option8">Option 8</option>
                            <option value="option9">Option 9</option>
                            <option value="option10">Option 10</option>
                            <option value="option11">Option 11</option>
                            <option value="option12">Option 12</option>
                        </select>
                        </div>                 
                    </li>
                    <li>
                      <?php dynamic_sidebar("sidebar-4"); ?>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- page title close -->
<section class="single-column">
  <div class="wrapper">
    <div id="primary" class="blog-post">
      <article>
          <div class="post-meta">
              <span class="author">Post by: <a href="#">Admin</a></span>
                <span class="comments">Comments<span>(2)</span></span>
            </div>
            <div class="post-thumb"><a href="blog-inner.php"><img src="images/blog-pic.jpg" alt="blog"></a></div>
            <div class="post-title-date">
              <div class="dates">
                  <div class="date-wrap">
                        <span class="pic-icon"></span>
                        <span class="date">17</span>
                    </div>
                    <span class="month">OCT 2013</span>
                </div>
                <div class="title">
                  <h2 class="post-title"><a href="blog-inner.php">Molestias excepturi sint facilis est et</a></h2>
                </div>
            </div>
            <div class="post-content">
              <p>Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo.</p>
        <div class="readmore"><a href="blog-inner.php">Read More</a></div>
            </div>
        </article>
        <article>
          <div class="post-meta">
              <span class="author">Post by: <a href="#">Admin</a></span>
                <span class="comments">Comments<span>(2)</span></span>
            </div>
            <div class="post-thumb"><a href="blog-inner.php"><img src="images/blog-pic.jpg" alt="blog"></a></div>
            <div class="post-title-date">
              <div class="dates">
                  <div class="date-wrap">
                        <span class="pic-icon"></span>
                        <span class="date">17</span>
                    </div>
                    <span class="month">OCT 2013</span>
                </div>
                <div class="title">
                  <h2 class="post-title"><a href="blog-inner.php">Molestias excepturi sint facilis est et</a></h2>
                </div>
            </div>
            <div class="post-content">
              <p>Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo.</p>
        <div class="readmore"><a href="blog-inner.php">Read More</a></div>
            </div>
        </article>
        <article>
          <div class="post-meta">
              <span class="author">Post by: <a href="#">Admin</a></span>
                <span class="comments">Comments<span>(2)</span></span>
            </div>
            <div class="post-thumb"><a href="blog-inner.php"><img src="images/blog-pic.jpg" alt="blog"></a></div>
            <div class="post-title-date">
              <div class="dates">
                  <div class="date-wrap">
                        <span class="pic-icon"></span>
                        <span class="date">17</span>
                    </div>
                    <span class="month">OCT 2013</span>
                </div>
                <div class="title">
                  <h2 class="post-title"><a href="blog-inner.php">Molestias excepturi sint facilis est et</a></h2>
                </div>
            </div>
            <div class="post-content">
              <p>Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo.</p>
        <div class="readmore"><a href="blog-inner.php">Read More</a></div>
            </div>
        </article>
        <article>
          <div class="post-meta">
              <span class="author">Post by: <a href="#">Admin</a></span>
                <span class="comments">Comments<span>(2)</span></span>
            </div>
            <div class="post-thumb"><a href="blog-inner.php"><img src="images/blog-pic.jpg" alt="blog"></a></div>
            <div class="post-title-date">
              <div class="dates">
                  <div class="date-wrap">
                        <span class="pic-icon"></span>
                        <span class="date">17</span>
                    </div>
                    <span class="month">OCT 2013</span>
                </div>
                <div class="title">
                  <h2 class="post-title"><a href="blog-inner.php">Molestias excepturi sint facilis est et</a></h2>
                </div>
            </div>
            <div class="post-content">
              <p>Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo.</p>
        <div class="readmore"><a href="blog-inner.php">Read More</a></div>
            </div>
        </article>
        <article>
          <div class="post-meta">
              <span class="author">Post by: <a href="#">Admin</a></span>
                <span class="comments">Comments<span>(2)</span></span>
            </div>
            <div class="post-thumb">
        <div class="carousel2" id="c1">
                  <a href="JavaScript:void(0);" class="prev" data-id="c1">111</a>
                    <a href="JavaScript:void(0);" class="next" data-id="c1">222</a>
                  <ul>
                      <li><a href="blog-inner.php"><img src="images/blog-pic.jpg" alt="blog"></a></li>
                        <li><a href="blog-inner.php"><img src="images/blog-pic.jpg" alt="blog"></a></li>
                    </ul>
                </div>
            </div>
            <div class="post-title-date">
              <div class="dates">
                  <div class="date-wrap">
                        <span class="video-icon"></span>
                        <span class="date">17</span>
                    </div>
                    <span class="month">OCT 2013</span>
                </div>
                <div class="title">
                  <h2 class="post-title"><a href="blog-inner.php">Molestias excepturi sint facilis est et</a></h2>
                </div>
            </div>
            <div class="post-content">
              <p>Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo.</p>
        <div class="readmore"><a href="blog-inner.php">Read More</a></div>
            </div>
        </article>
        <article>
          <div class="post-meta">
              <span class="author">Post by: <a href="#">Admin</a></span>
                <span class="comments">Comments<span>(2)</span></span>
            </div>
            <div class="post-thumb"><a href="blog-inner.php"><img src="images/blog-pic.jpg" alt="blog"></a></div>
            <div class="post-title-date">
              <div class="dates">
                  <div class="date-wrap">
                        <span class="pic-icon"></span>
                        <span class="date">17</span>
                    </div>
                    <span class="month">OCT 2013</span>
                </div>
                <div class="title">
                  <h2 class="post-title"><a href="blog-inner.php">Molestias excepturi sint facilis est et</a></h2>
                </div>
            </div>
            <div class="post-content">
              <p>Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo.</p>
        <div class="readmore"><a href="blog-inner.php">Read More</a></div>
            </div>
        </article>
        <article>
          <div class="post-meta">
              <span class="author">Post by: <a href="#">Admin</a></span>
                <span class="comments">Comments<span>(2)</span></span>
            </div>
            <div class="post-thumb"><a href="blog-inner.php"><img src="images/blog-pic.jpg" alt="blog"></a></div>
            <div class="post-title-date">
              <div class="dates">
                  <div class="date-wrap">
                        <span class="pic-icon"></span>
                        <span class="date">17</span>
                    </div>
                    <span class="month">OCT 2013</span>
                </div>
                <div class="title">
                  <h2 class="post-title"><a href="blog-inner.php">Molestias excepturi sint facilis est et</a></h2>
                </div>
            </div>
            <div class="post-content">
              <p>Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo.</p>
        <div class="readmore"><a href="blog-inner.php">Read More</a></div>
            </div>
        </article>
        <article>
          <div class="post-meta">
              <span class="author">Post by: <a href="#">Admin</a></span>
                <span class="comments">Comments<span>(2)</span></span>
            </div>
            <div class="post-thumb"><a href="blog-inner.php"><img src="images/blog-pic.jpg" alt="blog"></a></div>
            <div class="post-title-date">
              <div class="dates">
                  <div class="date-wrap">
                        <span class="pic-icon"></span>
                        <span class="date">17</span>
                    </div>
                    <span class="month">OCT 2013</span>
                </div>
                <div class="title">
                  <h2 class="post-title"><a href="blog-inner.php">Molestias excepturi sint facilis est et</a></h2>
                </div>
            </div>
            <div class="post-content">
              <p>Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo.</p>
        <div class="readmore"><a href="blog-inner.php">Read More</a></div>
            </div>
        </article>
        <article>
          <div class="post-meta">
              <span class="author">Post by: <a href="#">Admin</a></span>
                <span class="comments">Comments<span>(2)</span></span>
            </div>
            <div class="post-thumb"><a href="blog-inner.php"><img src="images/blog-pic.jpg" alt="blog"></a></div>
            <div class="post-title-date">
              <div class="dates">
                  <div class="date-wrap">
                        <span class="pic-icon"></span>
                        <span class="date">17</span>
                    </div>
                    <span class="month">OCT 2013</span>
                </div>
                <div class="title">
                  <h2 class="post-title"><a href="blog-inner.php">Molestias excepturi sint facilis est et</a></h2>
                </div>
            </div>
            <div class="post-content">
              <p>Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo.</p>
        <div class="readmore"><a href="blog-inner.php">Read More</a></div>
            </div>
        </article>
  </div>
    </div>
</section>
<section class="pagination gray">
<div class="cross-b top"><div class="cross white deco-bottom"></div></div>
  <div class="wrap">
      <div class="wrapper">
        <!-- pagination start -->
        <div class="wp-pagenavi">
          <a class="prevpostslink" href="#">Prev</a>
            <span class="current">1</span>
            <a class="page larger" href="#">2</a>
            <a class="page larger" href="#">3</a>
            <a class="page larger" href="#">4</a>
            <a class="nextpostslink" href="#">Next</a>
        </div>
        <!-- pagination close --> 
        </div>
    </div>
</section>
<?php get_footer(); ?>