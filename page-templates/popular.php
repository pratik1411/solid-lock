<?php
/**
 * Template Name: Popular Posts Page Template
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. The front page template
 * in Twenty Twelve consists of a page content area for adding text, images, video --
 * anything you'd like -- followed by front-page-only widgets in one or two columns.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>
<!-- page title start -->
<section class="page-title gray blog">
 <div class="header-cross">&nbsp;</div>
<div class="cross-a bottom"><div class="cross white deco-top"></div></div>
  <div class="wrap">
      <div class="wrapper">
          <h1>Solid Lock Tips</h1>
            <div class="widget-area">
              <ul class="widget-control">
                  <li><a href="<?php echo get_site_url(); ?>/popular-post">Popular Post</a></li>
                    <li><a href="<?php echo get_site_url(); ?>/recent-post">Recent Post</a></li>
                    <li>
                      <?php dynamic_sidebar("sidebar-5"); ?>                
                    </li>
                    <li>
                      <?php dynamic_sidebar("sidebar-4"); ?>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- page title close -->
<section class="single-column">
  <div class="wrapper">
    <div id="primary" class="blog-post">
    <?php 
    $paged = 1;
    $i = 1;
    if ( get_query_var('paged') ) $paged = get_query_var('paged');
    if ( get_query_var('page') ) $paged = get_query_var('page');
    $loop=new WP_Query(array("post_type"=>"post","orderby"=>"comment_count",'paged' => $paged, 'posts_per_page' => 9)); ?>
    <?php if ( $loop->have_posts() ) : ?>
      <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
        <?php get_template_part( 'content', get_post_format() ); ?>
      <?php endwhile; wp_reset_postdata(); ?>
    <?php endif; ?>
    </div>
    </div>
</section>
<section class="pagination gray">
<div class="cross-b top"><div class="cross white deco-bottom"></div></div>
  <div class="wrap">
      <div class="wrapper">
      <?php wp_pagenavi(array( 'query' => $loop ) ); ?>
    </div>
    </div>
</section>
<?php get_footer(); ?>