<?php
/**
 * Template Name: Gallery Page Template
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. The front page template
 * in Twenty Twelve consists of a page content area for adding text, images, video --
 * anything you'd like -- followed by front-page-only widgets in one or two columns.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>
<section class="page-title gray">
  <div class="header-cross">&nbsp;</div>
  <div class="cross-a bottom"><div class="cross white deco-top"></div></div>
  <div class="wrap">
      <div class="wrapper">
          <h1><?php the_title(); ?></h1>
        </div>
    </div>
</section>

<?php $i=1;?>
  <?php
    $image_gallery = get_post_meta( $post->ID, '_easy_image_gallery', true );
    $attachments = array_filter( explode( ',', $image_gallery ) );
    if ( $attachments ) {      
      $grid = array();
      
      do {
        $g = array();        
        if(!empty($attachments)) $g[] = array_shift($attachments);        
        if(!empty($attachments)) $g[] = array_shift($attachments);;
        $grid[] = $g;
      } while(!empty($attachments));

  ?>
<?php } ?>  

<?php $i=1; ?>
  <?php foreach ( $grid as $grids ) {  ?>
  <?php if($i%2!=0) { ?>
    <section class="gallery-page white">
    <?php if($i!=1) { ?>
    <div class="cross-a top"><div class="cross grays"></div></div>
    <?php } ?>
  <?php } else { ?>
    <section class="gallery-page gray">
    <div class="cross-b top"><div class="cross white deco-bottom"></div></div>
  <?php } ?>
  <div class="wrap">
      <div class="wrapper">
      <ul>
    <?php foreach($grids as $grid_id) { ?>
    
      <?php $image_attributes = wp_get_attachment_image_src( $grid_id,'full'  ); ?>
      <li> <?php echo wp_get_attachment_image( $grid_id, 'full' ); ?>  </li>
    
    <?php } ?>
      </ul>     
    </div>
    </div>
</section>
<?php $i++; ?>
   <?php } ?>

<!-- <section class="gallery-page gray">
<div class="cross-b top"><div class="cross white deco-bottom"></div></div>
  <div class="wrap">
      <div class="wrapper">
          <ul>
            <li><a href="#"><?php the_post_thumbnail(); ?></a></li>
          </ul>
        </div>
    </div>
</section> -->
<?php get_footer(); ?>