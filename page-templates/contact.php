<?php
/**
 * Template Name: Contact Page Template
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. The front page template
 * in Twenty Twelve consists of a page content area for adding text, images, video --
 * anything you'd like -- followed by front-page-only widgets in one or two columns.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>
<section class="map">
<div class="header-cross">&nbsp;</div>
  <div class="cross-b bottom"><div class="cross black"></div></div>
    <div class="wrap">
<div id="test1" class="gmap3"></div>
     <!-- <iframe width="100%" height="600" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.co.in/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=11%2F25+Foam+Street,+Elwood,+Victoria,+Australia&amp;aq=0&amp;oq=11%2F25+Foam+Street+Elwood&amp;sll=23.029562,72.566542&amp;sspn=0.036336,0.066047&amp;ie=UTF8&amp;hq=&amp;hnear=11%2F25+Foam+St,+Elwood+Victoria+3184,+Australia&amp;t=m&amp;ll=-37.877089,144.99404&amp;spn=0.023712,0.036564&amp;z=14&amp;iwloc=A&amp;output=embed"></iframe> -->
   </div>
</section>
<section class="contactus">
  <div class="wrap">
      <div class="wrapper">
          <?php while(have_posts()):the_post(); ?>
              <?php the_content(); ?>
          <?php endwhile; ?>
            <?php echo do_shortcode('[contact-form-7 id="52" title="Contact form 1"]'); ?>
        </div>
    </div>
</section>
<?php get_footer(); ?>
