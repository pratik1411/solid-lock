<?php
/**
 * The template for displaying Category pages
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
<section class="page-title gray blog">
 	<div class="header-cross">&nbsp;</div>
	<div class="cross-a bottom"><div class="cross white deco-top"></div></div>
  	<div class="wrap">
      	<div class="wrapper">
          <h1>Category Archives</h1>
        </div>
    </div>
</section>
<section class="single-column">
  <div class="wrapper">
    <div id="primary" class="blog-post">
		<?php if ( have_posts() ) : ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', get_post_format() ); ?>
			<?php endwhile; ?>
		<?php endif; ?>
		</div>
    </div>
</section>
<section class="pagination gray">
<div class="cross-b top"><div class="cross white deco-bottom"></div></div>
	<div class="wrap">
    	<div class="wrapper">
			<?php wp_pagenavi(); ?>
		</div>
    </div>
</section>
<?php get_footer(); ?>