<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme and one of the
 * two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
<section class="page-title gray blog">
 <div class="header-cross">&nbsp;</div>
<div class="cross-a bottom"><div class="cross white deco-top"></div></div>
  <div class="wrap">
      <div class="wrapper">
          <h1>Solid Lock Tips</h1>
            <div class="widget-area">
              <ul class="widget-control">
                  <li><a href="<?php echo get_site_url(); ?>/popular-post">Popular Post</a></li>
                    <li><a href="<?php echo get_site_url(); ?>/recent-post">Recent Post</a></li>
                    <li>
                      <?php dynamic_sidebar("sidebar-5"); ?>                 
                    </li>
                    <li>
                      <?php dynamic_sidebar("sidebar-4"); ?>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="single-column">
  <div class="wrapper">
    <div id="primary" class="blog-post">
		<?php if ( have_posts() ) : ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', get_post_format() ); ?>
			<?php endwhile; ?>
		<?php endif; ?>
		</div>
    </div>
</section>
<section class="pagination gray">
<div class="cross-b top"><div class="cross white deco-bottom"></div></div>
	<div class="wrap">
    	<div class="wrapper">
			<?php wp_pagenavi(); ?>
		</div>
    </div>
</section>

<?php get_footer(); ?>