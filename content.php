<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="post-meta">
    	<span class="author">Post by:<?php the_author(); ?></span>
        <span class="comments">Comments<span>(<?php echo comments_number('0','1','%'); ?>)</span></span>
    </div>
	<div class="post-thumb">
		<a href="<?php the_permalink(); ?>">
			<?php the_post_thumbnail("full"); ?>
		</a>
	</div>	
	<div class="post-title-date">
		<div class="dates">
	    	<div class="date-wrap">
	    		<?php $video=get_field("video"); ?>
	    		<?php if(empty($video)) { ?>
	            	<span class="pic-icon"></span>
	            <?php } else { ?>
	            	<span class="video-icon"></span>
	            <?php } ?>
	            <span class="date"><?php the_time('d'); ?></span>
	        </div>
	        <span class="month"><?php the_time('M Y'); ?></span>
	    </div>
	    <div class="title">
	    	<h2 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
	    </div>
	</div>
	<div class="post-content">
    	<p><?php echo get_excerpt("150"); ?></p>
		<div class="readmore"><a href="<?php the_permalink(); ?>">Read More</a></div>
    </div>
</article>