<?php
/**
 * The template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
<section class="page-title gray blog">
 <div class="header-cross">&nbsp;</div>
<div class="cross-a bottom"><div class="cross white deco-top"></div></div>
	<div class="wrap">
    	<div class="wrapper">
        	<h1>Solid Lock Tips</h1>
            <div class="widget-area">
            	<ul class="widget-control">
                	<li><a href="<?php echo get_site_url(); ?>/popular-post">Popular Post</a></li>
                    <li><a href="<?php echo get_site_url(); ?>/recent-post">Recent Post</a></li>
                    <li>
                    	<?php dynamic_sidebar("sidebar-5"); ?>                   
                    </li>
                    <li>
                    	<?php dynamic_sidebar("sidebar-4"); ?>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="single-column">
	<div class="wrapper">
      <div id="primary" class="blog-post inner">
		<article>
        	<div class="post-title-date">
            	<div class="dates">
                	<div class="date-wrap">
                        <span class="pic-icon"></span>
                        <?php while ( have_posts() ) : the_post(); ?>
                        <span class="date"><?php the_time('d'); ?></span>
                    </div>
                    <span class="month"><?php the_time('M Y'); ?></span>
                </div>
                <div class="title">
                	<h2 class="post-title"><?php the_title(); ?></h2>
                    <div class="post-meta">
                        <span class="author">Post by: <a href="#"><?php the_author(); ?></a></span>
                        <span class="comments">Comments<span>(<?php echo comments_number('0','1','%'); ?>)</span></span>
                    </div>
                </div>
            </div>
            <div class="post-content">
            	<div class="alignleft">
                    <?php the_content(); ?>
				<?php endwhile; ?>
                <div class="addthis_toolbox">
                 </div>
            </div>
        </article>    
      </div>
    </div>
</section>
<section class="pagination gray">
<div class="cross-b top"><div class="cross white deco-bottom"></div></div>
<div class="wrap">
    	<div class="wrapper">
            <div id="comments">
        	<div id="respond">
<?php comments_template(); ?>
</section>
<?php get_footer(); ?>