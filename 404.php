<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
<section class="page-title gray">
 <div class="header-cross">&nbsp;</div>
<div class="cross-a bottom"><div class="cross white deco-top"></div></div>
  <div class="wrap">
      <div class="wrapper">
          <h1><?php _e( 'Not found', 'twentythirteen' ); ?></h1>
        </div>
    </div>
</section>
<section class="single-column">
  <div class="wrapper">
		<h2><?php _e( 'This is somewhat embarrassing, isn&rsquo;t it?', 'twentythirteen' ); ?></h2>
		<p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'twentythirteen' ); ?></p>
		<?php get_search_form(); ?>
	</div>
</section>

		

<?php get_footer(); ?>