<?php
/**
 * The template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
<section class="page-title gray">
 <div class="header-cross">&nbsp;</div>
<div class="cross-a bottom"><div class="cross white deco-top"></div></div>
	<div class="wrap">
    	<div class="wrapper">
        	<h1><?php the_title(); ?></h1>
        </div>
    </div>
</section>
<section class="services-detail-page">
	<div class="wrap">
    	<div class="wrapper">
    		<?php while(have_posts()):the_post(); ?>
        		<?php the_content(); ?>
        	<?php endwhile; ?>
        </div>
    </div>
</section>

<?php get_footer(); ?>