<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
	<link href="<?php echo get_template_directory_uri(); ?>/css/base.css" type="text/css" rel="stylesheet">
	<link href="<?php echo get_template_directory_uri(); ?>/css/style.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.8.0.min.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery-ui-1.9.2.custom.min.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jcarousellite_1.0.1.js"></script>
	<script type="text/javascript">
	$(document).ready(function() {
		$('.carousel').each(function() {
	        $(this).jCarouselLite({
	          btnNext: $('a[data-id="'+$(this).attr('id')+'"].next'),
	          btnPrev: $('a[data-id="'+$(this).attr('id')+'"].prev'),
			  visible: $('.carousel li').length
	        });
	    });
	});
	</script>
	<script type="text/javascript">
	$(document).ready(function() {
		$('.carousel2').each(function() {
	        $(this).jCarouselLite({
	          btnNext: $('a[data-id="'+$(this).attr('id')+'"].next'),
	          btnPrev: $('a[data-id="'+$(this).attr('id')+'"].prev'),
			  visible: 1
	        });
	    });
	});
	</script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.cycle2.js"></script>
	<script type="text/javascript">
		$(function() {
			$(".cycle-slideshow div > img.attachment-full").each(function(i, elem) {
				var img = $(elem);
				$(this).hide();
				$(this).parent().css({
				background: "url(" + img.attr("src") + ") no-repeat center top",
				});
			});
		});
	</script>
<script src="http://maps.googleapis.com/maps/api/js?sensor=false" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/gmap3.min.js"></script> 
<script type="text/javascript">
      function Panorama(){
        var p,  marker, infowindow, map;
        
        this.setMap = function(obj){
          map = obj;
        }
        
        this.setMarker = function(obj){
          marker = obj;
        }
        
        this.setInfowindow = function(obj){
          infowindow = obj;
        }
        
        this.open = function(){
          infowindow.open(map, marker);
        }
        
        this.run = function(id){
          if (!marker) {
            return;
          }
          p = new google.maps.StreetViewPanorama(
            document.getElementById(id), 
            { navigationControl: true,
              navigationControlOptions: {style: google.maps.NavigationControlStyle.ANDROID},
              enableCloseButton: false,
              addressControl: false,
              linksControl: false
            }
          );
          p.bindTo("position", marker);
          p.setVisible(true);
        }
      };
    
      $(function(){
        
        var points = [
          [-37.885032,144.99112]
        ],
        map;
        
        $('#test1').gmap3({
          map:{
            options:{
              zoom: 14,
              mapTypeId: google.maps.MapTypeId.ROADMAP,
              streetViewControl: false,
              center: points[0]
            },
            callback: function(aMap){
              map = aMap;
            }
          }
        });
        $.each(points, function(i, point){
          var panorama = new Panorama();
          panorama.setMap(map);
        
          $("#test1").gmap3({
            marker:{
              latLng: point,
              options:{
				  	title: "Click to open", 
					draggable: true,
					icon: "<?php echo get_template_directory_uri(); ?>/images/map-arrow.png",
					},
              callback: function(marker){
                panorama.setMarker(marker);
              },
              events:{
                click: function(){
                  panorama.open();
                }
              }
            },
            infowindow:{
              options:{
                content: "<div class='infow'><h2>Solid Lock Locksmith</h2><div class='left'>11/25 Foam Street Elwood</div><div class='right'>Call Us Now! 0401073756 (24/7)<br>Email : <a href='mailto:solidlocklocksmiths@hotmail.com'>solidlocklocksmiths@hotmail.com</a></div></div>"
              },
              callback: function(infowindow){
                panorama.setInfowindow(infowindow);
              },
              events:{
                domready: function(){
                  panorama.run("iw"+i);
                }
              }
            }
          });
          
        });
          
      });
</script>
<!--For Multi select Start -->
	
<script type="text/javascript">
	function DropDown(el) {
		this.dd = el;
		this.opts = this.dd.find('ul.dropdown > li');
		this.val = [];
		this.index = [];
		this.initEvents();
	}
	DropDown.prototype = {
		initEvents : function() {
			var obj = this;

			obj.dd.on('click', function(event){
				$(this).toggleClass('active');
				event.stopPropagation();
			});

			obj.opts.children('label').on('click',function(event){
				var opt = $(this).parent(),
					chbox = opt.children('input'),
					val = chbox.val(),
					idx = opt.index();

				($.inArray(val, obj.val) !== -1) ? obj.val.splice( $.inArray(val, obj.val), 1 ) : obj.val.push( val );
				($.inArray(idx, obj.index) !== -1) ? obj.index.splice( $.inArray(idx, obj.index), 1 ) : obj.index.push( idx );
			});
		},
		getValue : function() {
			return this.val;
		},
		getIndex : function() {
			return this.index;
		}
	}

	$(function() {

		var dd = new DropDown( $('.dd') );

		$(document).click(function() {
			// all dropdowns
			$('.wrapper-dropdown-4').removeClass('active');
		});

	});
</script>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/fancybox/jquery.fancybox.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/fancybox/helpers/jquery.fancybox-media.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/fancybox/jquery.fancybox.css" media="screen" />

<script type="text/javascript">
$(document).ready(function () {
    $(".multi-select select").change(function () {
        var str = "";
        str = $(this).find(".multi-select select").text();
        $(this).next(".out").text(str);
    }).trigger('change');

    /*$(".various").click(function(e) {
    $.fancybox({
            'padding' : 0,
            'type' : 'swf',
            'href' : this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
            'swf' : { 'wmode' : 'transparent', 'allowfullscreen' : 'true' },
            'width' : '700px',
    });
    e.preventDefault();
  });*/
})
</script>
<script type="text/javascript">
$(document).ready(function() {
  $(".various").click(function(e) {
    $.fancybox({
            'padding' : 0,
            'type' : 'swf',
            'href' : this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
            'swf' : { 'wmode' : 'transparent', 'allowfullscreen' : 'true' }
        });
    e.preventDefault();
});
});  
</script>
    
<!--For Multi select Close -->

	<?php wp_head(); ?>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36114159-16']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
  
</head>

<body <?php body_class(); ?> onLoad="initialize()">
<header>
  <div class="wrap">
    <div class="wrapper">
      <span class="logo"><a href="<?php echo get_site_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="logo"></a></span>
      <div class="hright">
      	<div class="call-now">
        	<?php dynamic_sidebar("sidebar-2"); ?>
        </div>
        <nav>
          <?php wp_nav_menu(array('menu_class' => 'menu','link_before'=>'<span>','link_after'=>'</span>')); ?>
        </nav>
      </div>
    </div>
  </div>
</header>
