<?php
/**
 * The template for displaying Search Results pages
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
<section class="page-title gray blog">
 <div class="header-cross">&nbsp;</div>
<div class="cross-a bottom"><div class="cross white deco-top"></div></div>
  <div class="wrap">
  <div class="wrapper">
          <h1><?php printf( __( 'Search Results for: %s', 'twentythirteen' ), get_search_query() ); ?></h1>
        </div>
    </div>
</section>
<section class="single-column">
  <div class="wrapper">
    <div id="primary" class="blog-post">
<?php if ( have_posts() ) : ?>

			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', get_post_format() ); ?>
			<?php endwhile; ?>
		<?php else : ?>
			<?php  _e( '<h4>Try other keywords.</h4>', 'twentythirteen' ); ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>
</div>
    </div>
</section>
<?php get_footer(); ?>