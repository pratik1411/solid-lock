<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>
<footer>
	<div class="wrap">
    	<div class="wrapper">
        	<div class="social-media">
            	<a href="<?php echo get_option('fbid'); ?>" class="facebook"></a>
                <!--<a href="#" class="twitter"></a>
                <a href="#" class="in"></a>
                <a href="#" class="gplus"></a>
                <a href="#" class="rss"></a>-->
            </div>
            <?php dynamic_sidebar("sidebar-3"); ?>
            <div class="copy">
            	<div class="left">
                	© 2013  Solid Lock Locksmiths. All Rights Reserved.<br>Design & Developed by <a href="http://www.etrafficwebdesign.com.au">eTraffic Web Design</a>
                </div>
                <div class="right">
                	 <?php wp_nav_menu(); ?>
                </div>
            </div>
        </div>
    </div>
</footer>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/placeholder.js"></script>
<script type="text/javascript">
   var _mfq = _mfq || [];
   (function() {
       var mf = document.createElement("script"); mf.type = "text/javascript"; mf.async = true;
       mf.src = "//cdn.mouseflow.com/projects/69fb0708-5a06-455c-a5ca-5ddf0385a942.js";
       document.getElementsByTagName("head")[0].appendChild(mf);
   })();
</script>
<?php wp_footer(); ?>
</body>
</html>
